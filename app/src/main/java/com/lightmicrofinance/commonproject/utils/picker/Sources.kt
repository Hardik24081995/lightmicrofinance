package com.commonProject.utils.picker

enum class Sources {
    CAMERA, GALLERY, DOCUMENTS, CHOOSER
}