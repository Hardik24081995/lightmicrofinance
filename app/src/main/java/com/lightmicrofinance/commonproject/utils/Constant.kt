package com.commonProject.utils

object Constant {
    const val NA = "NA"
    const val ISCHECKED = "isChecked"
    const val LEAD = "lead"
    const val COLLECTION ="collection"
    const val PENDING ="pending"
    const val PARTIALY ="Partialy"
    const val COLLECTED ="Collected"
    const val TYPE ="type"
    const val SITE = "site"
    const val TICKET = "ticket"
    const val CUSTOMER = "customer"
    const val EMPLOYEE = "employee"
    const val VISITOR_ID = "visitor_id"
    const val CUSTOMER_ID = "customer_id"
    const val CUSTOMER_NAME = "customer_name"
    const val BASE_URL = "http://societyfy.in/lightmf/"
    const val API_URL = "${BASE_URL}api/"
    const val EMP_PROFILE = "${BASE_URL}assets/uploads/user/"
    const val TICKET_IMG = "${BASE_URL}assets/uploads/ticket/"
    const val PDF_INVOICE_URL = "${BASE_URL}assets/uploads/invoice/"
    const val PDF_QUOTATION_URL = "${BASE_URL}assets/uploads/estimation/"
    const val PDF_INSPECTION_URL = "${BASE_URL}assets/uploads/inspection/"
    const val DOCUMENT_URL = "${BASE_URL}assets/uploads/document/"
    const val CMS_URL ="${API_URL}service/getPage?PageName="

    const val OVERTIME = "overtime"
    const val LATEFINE = "latefine"
    const val PAGE_SIZE = 10

    const val TITLE = "title"

    const val TEN_MILISEC = 600000

    const val TEXT = "text"

    // Common Params
    const val METHOD = "method"
    const val BODY = "body"
    const val MESSAGE = "message"
    const val ERROR = "error"
    const val ROW_COUNT = "rowCount"
    const val DATA = "data"
    const val DATA1 = "data1"
    const val DATA_LEAD = "data_lead"
    const val DATA_SITE = "data_site"


    // ---Server Date Time--//
    const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"


    const val MOBILE = "mobile"
    const val SERVICE_ID = "serviceId"
    const val UNAUTHORIZED = "unauthorized"


    //----- Lead Type-----
    const val HOT = "Hot"
    const val WARM = "Warm"
    const val COLD = "Cold"
    const val SILENT = "Silent"


    //--- Method Name-----

    const val METHOD_LOGIN = "checkLogin"
    const val METHOD_COLLECTION_LIST = "checkLogin"

}